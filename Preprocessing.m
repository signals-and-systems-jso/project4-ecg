clc
clear
close all

%% Part1
load('Data');
Fs = data.samplingfreq;            % Sampling frequency                    
T = 1/Fs;             % Sampling period       
L = 4170;             % Length of signal
t = (0:L-1)*T;        % Time vector
figure
plot(t,data.signal)
title('Heart Beat Signal')
xlabel('t (ms)')
ylabel('X(t) (voltage)')

%% Part 2

Y = fft(data.signal);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;
figure
plot(f,P1) 
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
ly = length(Y);
fang = (-ly/2:ly/2-1)/ly*Fs;
phs = angle(fftshift(Y));
figure
plot(fang,phs/pi)
xlabel 'Frequency (Hz)'
ylabel 'Phase / \pi'
grid

%% Part 3

H = (f > 5);

figure
plot(f, H);

filtered = P1 .* H';
figure
plot(f,filtered)

%% P4 data file process aksare code marboot be in bakhsh az stackoverflow jam avari shode :)

x = data.signal;

fs = data.samplingfreq;             %#sampling rate
f0 = 50;                %#notch frequency
fn = fs/2;              %#Nyquist frequency
freqRatio = f0/fn;      %#ratio of notch freq. to Nyquist freq.

notchWidth = 0.1;       %#width of the notch

%Compute zeros
notchZeros = [exp( sqrt(-1)*pi*freqRatio ), exp( -sqrt(-1)*pi*freqRatio )];

%#Compute poles
notchPoles = (1-notchWidth) * notchZeros;

b = poly( notchZeros ); %# Get moving average filter coefficients
a = poly( notchPoles ); %# Get autoregressive filter coefficients

%#filter signal x
y = filter(b,a,x);

Y = fft(y);
P2 = abs(Y/L);
P1 = P2(1:L/2+1);
P1(2:end-1) = 2*P1(2:end-1);
f = Fs*(0:(L/2))/L;
figure
plot(f,P1) 
title('Single-Sided Amplitude Spectrum of X(t)')
xlabel('f (Hz)')
ylabel('|P1(f)|')
filtered = P1 .* H';
figure
plot(f,filtered)

%% part 5:
H = (f < 50);

finalfilter = filtered .* H';
figure
plot(f,finalfilter)

m = ifft(finalfilter);

figure
plot(f,m)

autocorr(m);